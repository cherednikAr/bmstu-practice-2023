package dev.slabuzov.baseproject.controller;


import dev.slabuzov.baseproject.dto.DataDto;
import dev.slabuzov.baseproject.dto.InputData;
import dev.slabuzov.baseproject.dto.Response;
import dev.slabuzov.baseproject.service.LoanService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RequiredArgsConstructor
@RestController
@RequestMapping("/loan-service")
public class OrderController {
    private final LoanService service;

    @GetMapping("/getTariffs")
    public ResponseEntity<Response> getTariffs(){
        Response response = new Response(new DataDto());
        response.data.setTariffs(service.tariffList());
        return ResponseEntity.ok(response);
    }

    @PostMapping (value = "/order")
    public ResponseEntity<Response> saveLoan(@RequestBody InputData input){
        Response response = new Response(new DataDto());
        String resultOrderId = service.saveOrder(input.getUserId(), input.getTariffId());
        response.data.setOrderId(resultOrderId);

        return ResponseEntity.ok(response);
    }

    @GetMapping("/getStatusOrder")
    public ResponseEntity<Response> getStatusOrder(@RequestParam("orderId") String orderId ){
        Response response = new Response(new DataDto());
        String resultStatus = service.getStatusByOrderId(orderId);
        response.data.setOrderStatus(resultStatus);
        return ResponseEntity.ok(response);
    }

    @DeleteMapping("deleteOrder")
    public HttpStatus deleteOrder(@RequestBody InputData input){
        service.delete(input.getUserId(), input.getOrderId());
        return HttpStatus.ACCEPTED;
    }
    private ResponseEntity<Response> defaultResponse() {
        return ResponseEntity.badRequest().build();
    }
}