package dev.slabuzov.baseproject.dao.impl;

import dev.slabuzov.baseproject.dao.TariffDao;
import dev.slabuzov.baseproject.model.Tariff;
import lombok.RequiredArgsConstructor;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
@RequiredArgsConstructor
public class TariffRepository implements TariffDao {
    private final JdbcTemplate jdbcTemplate;
    private static final String QUERY_TARIFF_FINDALL = "SELECT * FROM tariff";
    private static final String QUERY_TARIFF_EXISTS = "SELECT * FROM tariff WHERE id = ?";
    public Tariff mapRowToTariff(ResultSet rs, int rowNum) throws SQLException {
        Tariff tariff = new Tariff();
        tariff.setId(rs.getLong("id"));
        tariff.setType(rs.getString("type"));
        tariff.setInterest_rate(rs.getString("interest_rate"));
        return tariff;
    }

    @Override
    public List<Tariff> list() {
        return jdbcTemplate.query(QUERY_TARIFF_FINDALL, this::mapRowToTariff);
    }

    @Override
    public boolean absentTariff(Long tariffId) {
        List<Tariff> results = jdbcTemplate.query(QUERY_TARIFF_EXISTS, this::mapRowToTariff, tariffId);
        return results.isEmpty();
    }

}