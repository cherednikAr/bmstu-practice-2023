package dev.slabuzov.baseproject.dao.impl;

import dev.slabuzov.baseproject.dao.LoanDao;
import dev.slabuzov.baseproject.model.LoanOrder;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;

@Component
public class LoanRepository implements LoanDao {
    private final JdbcTemplate jdbcTemplate;

    public LoanRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }
    RowMapper<LoanOrder> rowMapper = (rs, rowNum) -> {
        LoanOrder order = new LoanOrder();

        order.setId(rs.getLong("id"));
        order.setOrder_id(rs.getString("order_id"));
        order.setUser_id(rs.getLong("user_id"));
        order.setTariff_id(rs.getLong("tariff_id"));
        order.setCredit_rating(rs.getDouble("credit_rating"));
        order.setStatus(rs.getString("status"));
        order.setTime_insert(rs.getTimestamp("time_insert"));
        order.setTime_update(rs.getTimestamp("time_update"));

        return order;
    };
    @Override
    public Optional<LoanOrder> getByOrderId(String order_id) {
        String sql = "select * from loan_order where  order_id =" + "'" + order_id + "'" ;
        return jdbcTemplate.query(sql, rowMapper).stream().findFirst();
    }

    @Override
    public LoanOrder create(LoanOrder order) {
        String sql = "insert into loan_order( order_id, user_id, tariff_id, credit_rating, status, time_insert) values( ?, ?, ?, ?, ?, ?) ";
        jdbcTemplate.update(sql,
                order.getOrder_id(),
                order.getUser_id(),
                order.getTariff_id(),
                order.getCredit_rating(),
                order.getStatus(),
                order.getTime_insert());
        return order;
    }

    @Override
    public Optional<LoanOrder> getStatusByUserTariffID(Long userId, Long tariffId) {
        String sql = "select * from loan_order where  user_id =" + "'" + userId + "'"+" and tariff_id =" +  "'" + tariffId + "'";
        return jdbcTemplate.query(sql, rowMapper).
                stream().findFirst();
    }

    @Override
    public Optional<LoanOrder> getByUserOrderID(Long userId, String orderId) {
        String sql = "select * from loan_order where user_id ="+"'" + userId + "'"+" and order_id ="+"'" + orderId + "'";
        return jdbcTemplate.query(sql, rowMapper).stream().findFirst();
    }

    @Override
    public void delete( String order_id) {
        String sql = "delete from loan_order where order_id="+"'" + order_id + "'";
        jdbcTemplate.query(sql, rowMapper);
    }

    @Override
    public List<LoanOrder> getByStatus(String status) {
        String sql = "select * from loan_order where status ="+"'" + status + "'" ;
        return jdbcTemplate.query(sql, rowMapper).stream().toList();
    }

    @Override
    public void updateTimeUpDateStatus(Timestamp timeUpDate, String status, String orderId) {
        String sql = "update loan_order set time_update = ?, status = ? where order_id = ?";
        jdbcTemplate.update(sql, timeUpDate,status, orderId);
    }
}