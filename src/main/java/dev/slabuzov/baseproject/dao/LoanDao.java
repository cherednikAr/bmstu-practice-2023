package dev.slabuzov.baseproject.dao;

import dev.slabuzov.baseproject.model.LoanOrder;

import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;

public interface LoanDao {
    LoanOrder create(LoanOrder order);
    Optional<LoanOrder> getByOrderId(String order_id);
    Optional<LoanOrder> getStatusByUserTariffID(Long userId, Long tariffId);
    Optional<LoanOrder> getByUserOrderID(Long userId, String orderId );
    void delete( String order_id);
    List<LoanOrder> getByStatus(String status);
    void updateTimeUpDateStatus(Timestamp timeUpDate, String status, String orderId);
}