package dev.slabuzov.baseproject.dao;

import dev.slabuzov.baseproject.model.Tariff;

import java.util.List;

public interface TariffDao {
    List<Tariff> list();
    boolean absentTariff(Long tariffId);
}
