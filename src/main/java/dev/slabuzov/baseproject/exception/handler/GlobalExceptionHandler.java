package dev.slabuzov.baseproject.exception.handler;

import dev.slabuzov.baseproject.dto.ResponseError;
import dev.slabuzov.baseproject.exception.ErrorObject;
import dev.slabuzov.baseproject.exception.GeneralException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

@ControllerAdvice
public class GlobalExceptionHandler {
    @ExceptionHandler(GeneralException.class)
    public ResponseEntity<ResponseError> handleTariffNotFoundException(GeneralException ex, WebRequest request){
        ResponseError response = new ResponseError(new ErrorObject());
        response.error.setCode(ex.getCode());
        response.error.setMessage(ex.getMessage());
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }
}