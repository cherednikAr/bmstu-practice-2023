package dev.slabuzov.baseproject.exception;

import lombok.Getter;

@Getter
public class GeneralException extends RuntimeException {
    private final String code;
    public GeneralException(String message, String code){
        super(message);
        this.code = code;
    }
}