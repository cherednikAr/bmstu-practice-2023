package dev.slabuzov.baseproject.exception;

import lombok.Data;

@Data
public class ErrorObject {
    private String code;
    private String message;
}