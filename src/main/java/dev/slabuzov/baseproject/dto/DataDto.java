package dev.slabuzov.baseproject.dto;

import dev.slabuzov.baseproject.model.Tariff;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DataDto {
    String orderId;
    String orderStatus;
    List<Tariff> tariffs;
}