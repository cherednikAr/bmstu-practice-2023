package dev.slabuzov.baseproject.dto;

import lombok.Data;

@Data
public class InputData {
    Long userId;
    Long tariffId;
    String orderId;

}