package dev.slabuzov.baseproject.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import dev.slabuzov.baseproject.exception.ErrorObject;
import lombok.Data;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
public class ResponseError {
    public final ErrorObject error;
}