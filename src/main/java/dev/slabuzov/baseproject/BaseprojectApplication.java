package dev.slabuzov.baseproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * Application entry point.
 */
@SpringBootApplication
@EnableScheduling
public class BaseprojectApplication {

    public static void main(String[] args) {
        SpringApplication.run(BaseprojectApplication.class, args);
    }

}
