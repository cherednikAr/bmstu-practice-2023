package dev.slabuzov.baseproject.service;

import dev.slabuzov.baseproject.dao.impl.LoanRepository;
import dev.slabuzov.baseproject.dao.impl.TariffRepository;
import dev.slabuzov.baseproject.exception.GeneralException;
import dev.slabuzov.baseproject.model.LoanOrder;
import dev.slabuzov.baseproject.model.Tariff;
import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.List;
import java.util.Objects;
import java.util.Random;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class LoanService {
    private final TariffRepository tariffRepository;
    private final LoanRepository loanRepository;

    private LoanOrder orderCreation(Long userId, Long tariffId){
        Random random = new Random();
        Double creditRating = random.nextInt(10, 90) / 100.0;
        LoanOrder created = new LoanOrder();

        created.setOrder_id(String.valueOf(UUID.randomUUID()));
        created.setUser_id(userId);
        created.setTariff_id(tariffId);
        created.setCredit_rating(creditRating);
        created.setStatus("IN_PROGRESS");
        created.setTime_insert(new Timestamp(System.currentTimeMillis()));
        return created;
    }
    private String RandomStatus(){
        Random random = new Random();
        return random.nextInt() % 2 == 0? "APPROVED" : "REFUSED";
    }

    public List<Tariff> tariffList(){
        return tariffRepository.list();
    }
    public String getStatusByOrderId(String orderId){
        LoanOrder status = loanRepository.getByOrderId(orderId).orElseThrow(() -> new GeneralException("Заявка не найдена", "ORDER_NOT_FOUND"));
        return status.getStatus();
    }

    public String saveOrder(Long userId, Long tariffId){
        if (tariffRepository.absentTariff(tariffId)) throw new GeneralException("Тариф не найден", "TARIFF_NOT_FOUND");
        var loanOrder = loanRepository.getStatusByUserTariffID(userId, tariffId);
        loanOrder.ifPresent(elem -> {
            String status = elem.getStatus();
            if (status.equals("IN_PROGRESS")) throw new GeneralException("Заявка в процессе рассмотрения", "LOAN_CONSIDERATION");
            if (status.equals("APPROVED")) throw new GeneralException("Кредит одобрен", "APPROVED");
            if (status.equals("REFUSED")) throw new GeneralException("Отказ в получении кредита", "TRY_LATER");
        });
        return loanRepository.create(orderCreation(userId, tariffId)).getOrder_id();
    }
    public void delete(Long userId, String order_id){
        LoanOrder status = loanRepository.getByUserOrderID(userId, order_id).orElseThrow(() -> new GeneralException("Заявка не найдена", "ORDER_NOT_FOUND"));
        if(Objects.equals(status.getStatus(), "IN_PROGRESS")){
            throw new GeneralException("Невозможно удалить заявку","ORDER_IMPOSSIBLE_TO_DELETE");
        }
        loanRepository.delete(order_id);
    }

    @Scheduled(fixedDelay = 120000)
    public void orderRevie(){
        List<LoanOrder> orders = loanRepository.getByStatus("IN_PROGRESS");
        orders.forEach(elem -> loanRepository.updateTimeUpDateStatus(new Timestamp(System.currentTimeMillis()), RandomStatus(), elem.getOrder_id()));
    }
}